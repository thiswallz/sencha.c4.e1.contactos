Ext.define('Ejemplo1Contactos.view.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'main',
    requires: [
        'Ext.TitleBar',
        'Ext.Video'
    ],
    config: {
        tabBarPosition: 'bottom',

        items: [
            {
                title: 'Contactos',
                iconCls: 'action',

                items: [
                    {
                        xtype: 'listacontactos'           
                    }
                ]
            }
        ]
    }
});
