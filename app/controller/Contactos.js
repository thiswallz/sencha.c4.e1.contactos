Ext.define('Ejemplo1Contactos.controller.Contactos', {
    extend: 'Ext.app.Controller',
    //load stores
    config: {
        refs: {
            listaContactos: {
                selector: 'listacontactos',
                autoCreate: true
            }
        },
        control: {
            listaContactos: {
                accionTab: 'llamarNumero'
            }        
        }
    },
    llamarNumero: function(view, record) {
        console.log(record.get('nombre'))
        console.log(record.get('numeros'))
        //OUT
        //[{"type":"mobile","value":"6965 4138","id":0,"pref":false},{"type":"other","value":"6965 4138","id":1,"pref":false}]

        //window.open('tel:+'+record.get('numero'));
    },
    launch: function() {
        console.log("button")
        var view = this.getListaContactos();
        var arr = new Array();
        var options = new ContactFindOptions();
        options.filter=""; 
        options.multiple=true
        var filter = ["displayName", "phoneNumbers"];
        navigator.contacts.find(filter, function(contacts) {
            console.log('init:'+contacts.length)
            for (var i=0; i<contacts.length; i++) {
                console.log(contacts[i].displayName)
                if(contacts[i].phoneNumbers.length>0){
                    console.log(contacts[i].phoneNumbers[0].value);
                }
                var obj = {
                    nombre: contacts[i].displayName,
                    numeros: contacts[i].phoneNumbers
                };
                arr.push(obj);
                
            }
            var stLista = view.down('#listaId');
            stLista.setData(arr);
            stLista.refresh();
        }, onError, options);

    }
});